﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace EFChap1
{
    public static class Recp3
    {
        public static void Receip3()
        {
         
          using(var context=new EF6RecipesEntities()){
              var artist = new Artist { FirstName="Alan",LastName="Jackson",MiddleName="AJ"};
              var album1 = new Album { AlbumName="Drive"};
              var album2 = new Album { AlbumName = "Live at Texas Stadium"};
              artist.Albums.Add(album1);
              artist.Albums.Add(album2);
              context.Artists.Add(artist);

              var artist1 = new Artist { FirstName = "Tobby", LastName = "Keith" };
              var artist2 = new Artist { FirstName = "Merle", LastName = "Haggard" };
              var album = new Album { AlbumName="Honkoyton Ubiversity"};
              artist1.Albums.Add(album);
              artist2.Albums.Add(album);
              context.SaveChanges();
              Console.WriteLine("Artists and their albums...");
              
              Console.WriteLine("{0} {1}",artist.FirstName,artist.LastName);
              artist.Albums.Select(it => new {it.AlbumName}).Print();

              Console.WriteLine("{0}",album.AlbumName);
              album.Artists.Select(it => new { name = it.FirstName + " " + it.LastName }).Print();
              Console.ReadKey();
          }
        }
    }
}
