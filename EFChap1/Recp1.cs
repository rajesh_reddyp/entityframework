﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EFChap1
{
    public static class Recp1
    {
        public static void Receip1() {
            using (var context = new EF6RecipesEntities())
            {
                var person = new Person { FirstName = "Robert", MiddleName = "Allen", LastName = "Doe", PhoneNumber = "867-5309" };
                context.People.Add(person);
                person = new Person { FirstName = "John", MiddleName = "K.", LastName = "Smith", PhoneNumber = "824-3031" };
                context.People.Add(person);
                person = new Person { FirstName = "Billy", MiddleName = "Albert", LastName = "Minor", PhoneNumber = "907-2212" };
                context.People.Add(person);
                person = new Person { FirstName = "Kathy", MiddleName = "Anne", LastName = "Ryan", PhoneNumber = "722-0038" };
                context.People.Add(person);
                context.SaveChanges();
                foreach (var prson in context.People)
                    Console.WriteLine("{0} {1} {2},Phone:{3}", prson.FirstName, prson.MiddleName, prson.LastName, prson.PhoneNumber);
                Console.ReadKey();
            }
        }
    }
}
