﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Chapter3
{
    class Program
    {
        static void Main(string[] args)
        {
            // EF6 Async
            //var asyncTask = AsyncEFRecipies.EF6AsyncDemo();
            //foreach (var c in AsyncEFRecipies.BusyChars()) {
            //    if (asyncTask.IsCompleted)
            //        break;
            //    Console.Write(c);
            //    Console.CursorLeft = 0;
            //    Thread.Sleep(100);
            //}

            // EF6 NativeSQL Statements
            Native_SQL_Statements.ExecuteNativeSQLStatements();
            Console.WriteLine("\nPress <enter> to continue...");
            Console.ReadLine();
        }
    }
}
