﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

namespace CodeFirstNewDatabaseSample
{
    class Program
    {
        static void Main(string[] args)
        {
           
            using (var db = new BloggingContext()) {
                Console.WriteLine("Enter a name for new Blog");
                var name = Console.ReadLine();
                var Blog = new Blog {Name=name};
                db.Blogs.Add(Blog);
                db.SaveChanges();
                var query = from b in db.Blogs
                            orderby b.Name
                            select b;
                Console.WriteLine("All blogs in the database");
                foreach (var blg in query)
                    Console.WriteLine(blg.Name);
                Console.WriteLine("Press Any Key To Exit");
                Console.ReadLine();
            }
        }
    }
}
