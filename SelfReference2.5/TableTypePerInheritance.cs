﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfReference2._5
{
    public static class TableTypePerInheritance
    {
        public static void Recipe8() {
            using (var context = new EF6RecipesContext()) {
                var business = new Business { Name = "Corner Dry Cleaning", LicenseNumber = "100x1" };
                context.Business.Add(business);
                var retail = new Retail { Name = "Shop And Save", LicenseNumber = "200C", Address = "101 Main", City = "AnyTown", State = "TX", ZIPCode = "76106" };
                context.Business.Add(retail);
                var web = new eCommerce { Name = "BuyNow.com", LicenseNumber = "300AB", URL = "www.buynow.com" };
                context.Business.Add(web);
               
                context.SaveChanges();
             }
            using (var context = new EF6RecipesContext()) {
                Console.WriteLine("\n---All Business ---");
                foreach (var b in context.Business)
                    Console.WriteLine("{0} (#{1})",b.Name,b.LicenseNumber);
                Console.WriteLine("\n Retail Business");
                foreach (var r in context.Business.OfType<Retail>())
                {
                    Console.WriteLine("{0} (#{1})", r.Name, r.LicenseNumber);
                    Console.WriteLine("{0}",r.Address);
                    Console.WriteLine("{0},{1} {2}",r.City,r.State,r.ZIPCode);
                }
                Console.WriteLine("\n eCommerce Business");
                foreach (var e in context.Business.OfType<eCommerce>()) {
                    Console.WriteLine("{0} (#{1})", e.Name, e.LicenseNumber);
                    Console.WriteLine("Online address is :{0}",e.URL);
                }
            }

        }
    }
}
