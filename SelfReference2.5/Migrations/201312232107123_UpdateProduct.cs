namespace SelfReference2._5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateProduct : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "Chapter2.Poduct", newName: "Product");
        }
        
        public override void Down()
        {
            RenameTable(name: "Chapter2.Product", newName: "Poduct");
        }
    }
}
