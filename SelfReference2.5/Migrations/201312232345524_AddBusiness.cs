namespace SelfReference2._5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusiness : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Chapter2.Business",
                c => new
                    {
                        BusinessId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        LicenseNumber = c.String(),
                    })
                .PrimaryKey(t => t.BusinessId);
            
            CreateTable(
                "Chapter2.eCommerce",
                c => new
                    {
                        BusinessId = c.Int(nullable: false),
                        URL = c.String(),
                    })
                .PrimaryKey(t => t.BusinessId)
                .ForeignKey("Chapter2.Business", t => t.BusinessId)
                .Index(t => t.BusinessId);
            
            CreateTable(
                "Chapter2.Retail",
                c => new
                    {
                        BusinessId = c.Int(nullable: false),
                        Address = c.String(),
                        City = c.String(),
                        State = c.String(),
                        ZIPCode = c.String(),
                    })
                .PrimaryKey(t => t.BusinessId)
                .ForeignKey("Chapter2.Business", t => t.BusinessId)
                .Index(t => t.BusinessId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Chapter2.Retail", "BusinessId", "Chapter2.Business");
            DropForeignKey("Chapter2.eCommerce", "BusinessId", "Chapter2.Business");
            DropIndex("Chapter2.Retail", new[] { "BusinessId" });
            DropIndex("Chapter2.eCommerce", new[] { "BusinessId" });
            DropTable("Chapter2.Retail");
            DropTable("Chapter2.eCommerce");
            DropTable("Chapter2.Business");
        }
    }
}
