namespace SelfReference2._5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPhotograph : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Chapter2.Photograph",
                c => new
                    {
                        PhotoId = c.Int(nullable: false, identity: true),
                        Title = c.String(),
                        ThumbnailBits = c.Binary(),
                        HighResolutionBits = c.Binary(),
                    })
                .PrimaryKey(t => t.PhotoId);
            
        }
        
        public override void Down()
        {
            DropTable("Chapter2.Photograph");
        }
    }
}
