namespace SelfReference2._5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddProduct : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Chapter2.Poduct",
                c => new
                    {
                        SKU = c.Int(nullable: false),
                        Description = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.SKU);
            
            CreateTable(
                "Chapter2.ProductWebInfo",
                c => new
                    {
                        SKU = c.Int(nullable: false),
                        ImageURL = c.String(),
                    })
                .PrimaryKey(t => t.SKU)
                .ForeignKey("Chapter2.Poduct", t => t.SKU)
                .Index(t => t.SKU);
            
        }
        
        public override void Down()
        {
            DropForeignKey("Chapter2.ProductWebInfo", "SKU", "Chapter2.Poduct");
            DropIndex("Chapter2.ProductWebInfo", new[] { "SKU" });
            DropTable("Chapter2.ProductWebInfo");
            DropTable("Chapter2.Poduct");
        }
    }
}
