namespace SelfReference2._5.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateeCommerecAndRetail : DbMigration
    {
        public override void Up()
        {
            DropColumn("Chapter2.eCommerce", "eCommerceId");
            DropColumn("Chapter2.Retail", "RetailId");
        }
        
        public override void Down()
        {
            AddColumn("Chapter2.Retail", "RetailId", c => c.Int(nullable: false, identity: true));
            AddColumn("Chapter2.eCommerce", "eCommerceId", c => c.Int(nullable: false, identity: true));
        }
    }
}
