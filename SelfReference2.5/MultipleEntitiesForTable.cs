﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfReference2._5
{
    public static class MultipleEntitiesForTable
    {
        public static void TableWithMultipleEntities() {
            //using (var context = new EF6RecipesContext()) {
            //    var photo = new Photograph { Title="My Dog",ThumbnailBits=new byte[100]};
            //     photo.PhotographFullImage=new PhotographFullImage { HighResolutionBits=new byte[2000]};
            //    context.Photographs.Add(photo);
            //    context.SaveChanges();
            //}

            using (var con = new EF6RecipesContext()) {

                var ab = con.Photographs.ToList();
                
                foreach (var pht in con.Photographs){
                   Console.WriteLine("Photo: {0}, Thumbnailsize {1} bytes", pht.Title, pht.ThumbnailBits.Length);
                   con.Entry(pht).Reference(p => p.PhotographFullImage).Load();
                   Console.WriteLine("Full Image Size: {0} bytes", pht.PhotographFullImage.HighResolutionBits.Length);
                }
                  
            }
        }
    }
}
