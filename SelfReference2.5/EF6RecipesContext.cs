﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfReference2._5
{
    public class EF6RecipesContext:DbContext
    {

        public DbSet<PictureCategory> PictureCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Photograph> Photographs { get; set; }
        public DbSet<PhotographFullImage> PhotographFullImages { get; set; }

        public DbSet<Business> Business { get; set; }

        public EF6RecipesContext()
            : base("EF6CodeFirstRecipesContext")
        { 
          
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<PictureCategory>().HasMany(cat => cat.Subcategories).WithOptional(cat => cat.ParentCategory);
           // splitting a entity into two tables
            modelBuilder.Entity<Product>().Map(m => { m.Properties(p => new { p.SKU, p.Price, p.Description }); m.ToTable("Product", "Chapter2"); }).Map(m => { m.Properties(p => new { p.SKU, p.ImageURL }); m.ToTable("ProductWebInfo", "Chapter2"); });
           // multiple entites for a single table 
            modelBuilder.Entity<Photograph>().HasRequired(p => p.PhotographFullImage).WithRequiredPrincipal(p => p.Photograph);
            modelBuilder.Entity<Photograph>().ToTable("Photograph","Chapter2");
            modelBuilder.Entity<PhotographFullImage>().ToTable("Photograph", "Chapter2");
        }


    }
}
