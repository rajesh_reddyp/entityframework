﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SelfReference2._5
{
    public static class PicCategory
    {
        public static void Recip5() {
            using (var context = new EF6RecipesContext()) {
                var Lovre = new PictureCategory { Name="Lovre"};
                var child = new PictureCategory { Name="Egyptian Antiquities"};
                Lovre.Subcategories.Add(child);
                child = new PictureCategory { Name="Sculptures"};
                Lovre.Subcategories.Add(child);
                child = new PictureCategory { Name = "Paintings" };
                Lovre.Subcategories.Add(child);
                var Paris = new PictureCategory { Name="Paris"};
                Paris.Subcategories.Add(Lovre);
                var vacation = new PictureCategory { Name="Summer Vacation"};
                vacation.Subcategories.Add(Paris);
                context.PictureCategories.Add(vacation);
                context.SaveChanges();
                var roots = context.PictureCategories.Where(c => c.ParentCategory == null).ToList();
                roots.ForEach(root=>Print(root,0));
            }
        }

        public static void Print(PictureCategory cat, int level){
            StringBuilder s = new StringBuilder();
            Console.WriteLine("{0} {1}",s.Append(' ',level).ToString(),cat.Name);
            cat.Subcategories.ForEach(child=>Print(child,level+1));
        }

    }
}
